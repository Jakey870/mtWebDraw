﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using mtTools;
using System.Text;

namespace mtWebDraw.Controllers
{
    public class ToolsController : Controller
    {

        #region 简地址跳转Action

        /// <summary>
        /// 简地址跳转Action
        /// </summary>
        /// <param name="Type"></param>
        /// <returns></returns>
        public ActionResult RedirectUrl(int Type)
        {
            string url = "";

            if (Type == 1)
                url = "/Content/Canvas/archive/grapheditor.bak/grapheditor.html";

            if (url.IsNullOrWhiteSpace())
                return Content("跳转类型未知！");

            return Redirect(url);
        }

        #endregion

        #region 清除全部缓存数据

        /// <summary>
        /// 清除全部缓存数据
        /// </summary>
        /// <returns></returns>
        public ActionResult ClearAllCache()
        {
            if (Request.UserHostAddress == "127.0.0.1" || Request.UserHostAddress == "::1")
            {
                CacheHelper.RemoveAllCache();
                return Content("OK");
            }
            return Content("请于服务器上进行此操作！");
        }

        #endregion

        #region 保存到桌面

        /// <summary>
        /// 保存到桌面：兼容各浏览器名称
        /// </summary>
        /// <param name="cname">保存的名称</param>
        /// <param name="htmlOutHtml">页面的outHtml内容</param>
        [ValidateInput(false)]
        public void SavaDesktop(string cname, string htmlOutHtml)
        {
            Encoding encoding;

            string outputFileName = "";
            string browser = Request.UserAgent.ToUpper();
            if (browser.Contains("MS") == true && browser.Contains("IE") == true)
            {
                outputFileName = HttpUtility.UrlEncode(cname);
                encoding = System.Text.Encoding.Default;
            }
            else if (browser.Contains("FIREFOX") == true)
            {
                outputFileName = cname;
                encoding = System.Text.Encoding.GetEncoding("GB2312");
            }
            else
            {
                outputFileName = HttpUtility.UrlEncode(cname);
                encoding = System.Text.Encoding.Default;
            }

            string type = Request["type"];

            string ex = "html";
            if(!type.IsNullOrWhiteSpace())
            {
                if (type == "1")
                    ex = "txt";
            }

            Response.Clear();
            Response.Buffer = true;
            Response.ContentEncoding = encoding;
            Response.AddHeader("Content-Disposition", "attachment;filename=" + outputFileName + "." + ex);
            Response.ContentType = "text/html";
            string message = htmlOutHtml;
            Response.Write(message);
            Response.End();
        }

        #endregion

    }
}